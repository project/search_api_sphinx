Sphinx backend for the Search API module
========================================

It uses Sphinx real-time (RT) indexes and sphinxQL.

Requirements
------------

- Drupal 7
- Search API module
- Sphinx 2.0.3+
- PHP 5.3+


Supported features
------------------

- Search API facets
- Search API facets OR
- Search API MLT ('More Like This')

Install Notes
-------------

When creating a Search API Sphinx Server, you need to indicate
a base directory in which the server path structure will be created.

Ex: /var/sphinx/

The user running your application process (ex:apache) must have full
access to this directory.

Notes
-----

- Distributed indexes (http://sphinxsearch.com/docs/current.html#distributed)
  are not yet supported.

- Custom configuration options can be added through the "custom settings" field.

- Creating/changing Search API indexes requires to stop and start
  the sphinx daemon to reload the modified configuration
  and so this is controlled by this module with the implication that
  the sphinx daemon has to be on the same server as the web application.
  This restriction may be somewhat lifted in future release.

- In case of crash. You can restart the daemon(s) through the interface.

- Several daemons can run simultaneously without problem.
  Just make sure they listen to different HOST:PORT.

- Fields are prefixed with "sphinx_" and ":" is changed to "___".
  Useful if you wish to use the extended query syntax ("direct" parse mode).
  Ex: vocabulary:machine_name => sphinx_vocabulary___machine_name.

- Note that the option "Search filter" for the "Search: Full text" filter
  for views is not supported.

- Nested fields (related fields) are not fully supported yet.
  Only related fields of type 'text' (list<text>) are supported
  and only for only 1 nested level.

Drush
-----

There is a special dush command "search-api-sphinx-fast-indexing" that takes
the name of a Search API index (using a Search API Sphinx server) as parameter.

This function allows to index a huge number of documents much faster than doing
so with the normal Search API indexing method.

It has some limitations and only **bundle** data alteration and to some extent
**aggretated fields** as well are supported. Exotic fields are not supported.

If your index fits into those limitations, you can then run the command:

> drush search-api-sphinx-fast-indexing indexname

It will index the entities of the corresponding Search API index.

To understand the benefits: indexing 400,000+ nodes with 15 fields
took more than 1h30 using the conventional Search API method while
it took 5 minutes using this method.

TODO
----

- Support other Search API features.
- Add an option to disable start/stop control by the application.
- Use TRUNCATE and OPTIMIZE commands once they are implemented
  in an official sphinx release.
