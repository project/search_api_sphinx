<?php
/**
 * @file
 * Drush command to start/stop the sphinx daemon associated to a server.
 */

/**
 * Implements hook_drush_command().
 */
function search_api_sphinx_drush_command() {
  $items = array();

  $items['search-api-sphinx-daemon'] = array(
    'callback' => 'drush_search_api_sphinx_daemon',
    'description' => dt('Start or stop the sphinx daemon.'),
    'drupal dependencies' => array('search_api_sphinx'),
    'arguments' => array(
      'server_id' => dt('The numeric ID or machine name of a sphinx server.'),
      'command' => dt("Command: 'start' or 'stop'"),
    ),
  );

  $items['search-api-sphinx-fast-indexing'] = array(
    'callback' => 'drush_search_api_sphinx_fast_indexing',
    'description' => dt('Fast indexing.'),
    'drupal dependencies' => array('search_api_sphinx'),
    'arguments' => array(
      'index_id' => dt('The numeric ID or machine name of a Search API index using a sphinx server.'),
    ),
  );

  $items['search-api-sphinx-clear-cache'] = array(
    'callback' => 'drush_search_api_sphinx_clear_cache',
    'description' => dt('Clear the queries cache.'),
    'drupal dependencies' => array('search_api_sphinx'),
    'arguments' => array(
      'index_id' => dt('The numeric ID or machine name of a Search API index using a sphinx server.'),
    ),
  );

  return $items;
}

/**
 * Start or stop the sphinx daemon.
 *
 * Note: for this to work properly the user executing the drush command
 * has to be the same as your application's (example: apache).
 */
function drush_search_api_sphinx_daemon($server_id = '', $command = 'start') {
  if (empty($server_id)) {
    drush_log('Please indicate a server id.', 'error');
  }
  elseif (search_api_sphinx_daemon($server_id, $command) !== 0) {
    drush_log('Error while starting the sphinx daemon.', 'error');
  }
  else {
    drush_log('Sphinx daemon started successfully.', 'success');
  }
}

/**
 * Launch a fast indexing on the given index.
 *
 * @See SearchApiSphinxService::fastIndexing()
 */
function drush_search_api_sphinx_fast_indexing($index_id = '') {
  $result = FALSE;
  $index = search_api_index_load($index_id);
  if (isset($index)) {
    $server = search_api_index_get_server($index);
    if (isset($server, $server->class) && $server->class === 'search_api_sphinx_service') {
      $result = $server->fastIndexing($index);
    }
  }

  if (!empty($result)) {
    drush_log('Fast indexing successful.', 'success');
  }
  else {
    drush_log('Fast indexing failed.', 'error');
  }
}

/**
 * Clear the queries cache for the given index.
 */
function drush_search_api_sphinx_clear_cache($index_id = '') {
  $result = FALSE;
  $index = search_api_index_load($index_id);
  if (isset($index)) {
    $server = search_api_index_get_server($index);
    if (isset($server, $server->class) && $server->class === 'search_api_sphinx_service') {
      cache_clear_all($index->machine_name . '_', 'cache_search_api_sphinx', TRUE);
      drush_log('Cache clearing successful.', 'success');
      return;
    }
  }
  drush_log('Cache clearing failed. Invalid index or not using a sphinx server.', 'error');
}
